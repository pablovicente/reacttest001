import React, { Component } from 'react';
import Draggable from 'react-draggable';
import '../css/Sticky.css';



class Sticky extends Component {
  constructor(props){
    super(props);

    this.state = {
      draggingSticky: false
    }
  }

  handleDrag(event){
    this.setState({draggingSticky: true});
  }

  handleStop(stickyId, event){
    this.props.updateHandler(stickyId, event.clientX, event.clientY);
  }

  openSticky(stickyId, event){
    // Hacky code to avoid the firing of both Drop and Click; Need to learn if there's a better way...
    this.setState({draggingSticky: false});
    if(this.state.draggingSticky){
      event.preventDefault();
    } else {
      console.log(`Rotate and Zoom with CSS3 div: ${stickyId}`);
    }
  }


  render() {
    return (
      <div className="stickiesContainer">
        {
          this.props.stickies.map((anSticky) => {
            return (
              <Draggable onDrag={this.handleDrag.bind(this)}
                         onStop={this.handleStop.bind(this, anSticky.cuid)}
                         key={anSticky.cuid}
              >
                <div
                  className="sticky"
                  key={anSticky.cuid}
                  style={{
                    float: 'left',
                    position: 'relative',
                    left: anSticky.left,
                    top: anSticky.top
                  }}
                  onClick={this.openSticky.bind(this, anSticky.cuid)}
                >
                  {anSticky.content}
                </div>
              </Draggable>
            )
          })
        }
      </div>
    )
  }
}

export default Sticky;