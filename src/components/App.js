import React, { Component } from 'react';
import cuid from 'cuid';
import Sticky from './Sticky';
import '../css/App.css';

class App extends Component {
  constructor(props){
    super(props);

    this.state = {
      stickies: [],
      newStickyText: '',
      addingNewSticky: false
    }
  }

  avoidFormSubmit(event){
    event.preventDefault();
  }

  newStickyContent(event){
    this.setState({newStickyText: event.target.value});
    this.setState({addingNewSticky:true});
  }

  addSticky(){
    // This might be all the info we need for a sticky based on spec
    const newSticky = {
      cuid: cuid(),
      x: 0,
      y: 0,
      closed: true,
      content: this.state.newStickyText
    };
    // Update state
    this.setState({ stickies: this.state.stickies.concat(newSticky), addingNewSticky: false, newStickyText:'' })
  }

  stickyUpdated(stickyId, x, y){
    console.log('sticky updated in child componen!');
    const currentStickiesState = this.state.stickies;

    currentStickiesState.forEach((currentSticky, idx) => {
      if(currentSticky.cuid === stickyId){
        currentStickiesState[idx].x = x;
        currentStickiesState[idx].y = y;
      }
    });
    this.setState({stickies: currentStickiesState});

  }

  render() {
    return (
      <div className="App">
        <div className="App-header">
          <h2>Stickies!</h2>
        </div>
        <form onSubmit={this.avoidFormSubmit}>
          <textarea value={this.state.newStickyText} style={{width:'200px', height:'50px'}} onChange={this.newStickyContent.bind(this)}>
          </textarea>
          <br/>
          <button onClick={this.addSticky.bind(this)}>Add</button>
        </form>
        <Sticky stickies={this.state.stickies} updateHandler={this.stickyUpdated.bind(this)}/>
      </div>
    );
  }
}

export default App;
