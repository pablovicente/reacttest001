Stickies Demo
=============

## Pre-requisites
* Nodejs
* npm 
* yarn 


## Usage

* Clone the repo
* npm install 
* npm start

## Current Suported Features
 
* Create new Sticky
* Drad and Drop Stikies (and update state with position)
* Listen to click event for opening the Sticky
 
## Working on  

* CSS transitions for opening the Sticky

## Tested in: 
* Chrome 
* Safari 
* Firefox 
* I don't have access to an IE browser or Windows Machine currently for testing